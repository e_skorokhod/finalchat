﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FinalChat.WEB.Models
{
    public class DialogVM
    {
       public int ID { get; set; }

        public int CompanionID { get; set; }

       public int LastmessageToID { get; set; }

       public byte[] CompanionAvatar { get; set; }

       public string CompanionFirstName { get; set; }

       public string LastMessage { get; set; }
       
        public bool LastMessageWritten { get; set; }

       
        public string LastMessageTime { get; set; }

    }
}
