﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinalChat.WEB.Models
{
    public class UserRealtionsVM
    {
        public UserVM User { get; set; }

        public IEnumerable<UserVM> searchResult = new List<UserVM>();

        public IEnumerable<UserVM> friends = new List<UserVM>();

        public IEnumerable<UserVM> outcomingInvites = new List<UserVM>();

        public IEnumerable<UserVM> incomingInvites = new List<UserVM>();

        public IEnumerable<int> blackListIDs = new List<int>();

        public IEnumerable<int> blocksofUserIDs = new List<int>();



    }
}
