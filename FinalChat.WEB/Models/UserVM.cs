﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FinalChat.WEB.Models
{
    public class UserVM
    {
        public int ID { get; set; }

        public string Login { get; set; }

        public string Email { get; set; }

        public bool ConfirmEmail { get; set; }

        [DataType(DataType.Password)]
        public string Password { get; set; }

     
        [DataType(DataType.Password)]
        [Remote(action: "CheckOldPassword", controller: "Home", ErrorMessage = "Incorrect old password.")]      
        public string OldPassword { get; set; }

        public byte[] PasswordHash { get; set; }

        public byte[] PasswordSalt { get; set; }

        [Display(Name = "First name")]
        [Required]
        public string FirstName { get; set; }

        [Display(Name = "Last name")]
        [Required]
        public string LastName { get; set; }

        [Required]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
        [DataType(DataType.Date)]
        public DateTime BirthDate { get; set; }

        public string Phone { get; set; }

        public  IFormFile AvatarFile { get; set; }

        public byte[] Avatar { get; set; }

        public DateTime RegistrationDate { get; set; }



        public bool NetworkStatus { get; set; }
        public int Gender { get; set; }
        public int MartialStatus { get; set; }

    }
}
