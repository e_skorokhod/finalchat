﻿using FinalChat.BBL.DTO;
using FinalChat.BBL.Interfaces;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinalChat.WEB.Hubs
{
    public class ChatHub : Hub
    {
        static List<UserDetails> ConnectedUsers = new List<UserDetails>();
        static List<MessageDetail> Messages = new List<MessageDetail>();
        private IUserService _userService;
        private IMessageService _messageService;


        public ChatHub(IUserService userService, IMessageService chatService)
        {
            _userService = userService;
            _messageService = chatService;
        }

        public void Connect(string UserName, int UserId)
        {
            var newUser = _userService.GetUserByID(UserId);
            var id = Context.ConnectionId;

            if (ConnectedUsers.Count(x => x.ConnectionID == id) == 0)
            {
                ConnectedUsers.Add(new UserDetails { ConnectionID = id, UserName =newUser.FirstName, UserID = newUser.ID, UserAvatar=newUser.Avatar });
            }

            UserDetails CurrentUser = ConnectedUsers.Where(x => x.ConnectionID == id).FirstOrDefault();

            //await Clients.Caller.SendAsync("onConnected", CurrentUser.UserID.ToString(), CurrentUser.UserName);

            //await Clients.AllExcept(CurrentUser.ConnectionID).SendAsync("onNewUserConnected", CurrentUser.UserID.ToString(), CurrentUser.UserName);
        }


        public async Task SendDirectMessage(string toUserId, string message,string dialogId)
        {
            

                string fromConnectionId = Context.ConnectionId;
                string fromUserId = (ConnectedUsers.Where(x => x.ConnectionID == Context.ConnectionId).Select(x => x.UserID).FirstOrDefault().ToString());
                int _fromUserId = 0;
                int.TryParse(fromUserId, out _fromUserId);
                int _toUserId = 0;
                int.TryParse(toUserId, out _toUserId);
                List<UserDetails> FromUsers = ConnectedUsers.Where(x => x.UserID == _fromUserId).ToList();
                List<UserDetails> ToUsers = ConnectedUsers.Where(x => x.UserID == _toUserId).ToList();

            await _messageService.SendMessageAsync(new MessageDTO
            {
                DialogID = Convert.ToInt32(dialogId),
                FromID = _fromUserId,
                ToID = _toUserId,
                MessageDate = DateTime.Now,
                MessageText = message,
                Written = false,
                FilePath = null
            });

            List<string> noDialogUsers = ConnectedUsers.Where(val => val.ConnectionID != FromUsers[0].ConnectionID &
                    val.ConnectionID != ToUsers[0].ConnectionID).Select(val => val.ConnectionID).ToList();

                

                if (FromUsers.Count != 0 && ToUsers.Count != 0)
                {
                    
                    await Clients.AllExcept(noDialogUsers).SendAsync("sendDirectMessage", FromUsers[0].UserID,message,DateTime.Now, FromUsers[0].UserAvatar,dialogId);            
                }

          
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            var userDisconnect = ConnectedUsers.FirstOrDefault(x => x.ConnectionID == Context.ConnectionId);
            if (userDisconnect != null)
            {
                ConnectedUsers.Remove(userDisconnect);
                if (ConnectedUsers.Count(x => x.UserID == userDisconnect.UserID) == 0)
                {
                    var id = userDisconnect.UserID.ToString();
                    Clients.All.SendCoreAsync("OnDisconnected", new object[] { id, userDisconnect.UserName }).Wait();
                }
            }
            return base.OnDisconnectedAsync(exception);
        }
    }
}
