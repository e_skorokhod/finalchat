﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinalChat.WEB.Hubs
{
    public class UserDetails
    {
        public string ConnectionID { get; set; }

        public int UserID { get; set; }

        public string UserName { get; set; }

        public byte[] UserAvatar { get; set; }
    }
}
