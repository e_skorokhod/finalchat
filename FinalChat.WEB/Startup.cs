﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using FinalChat.BBL.DTO;
using FinalChat.BBL.Interfaces;
using FinalChat.BBL.Services;
using FinalChat.DAL.EF;
using FinalChat.DAL.Entities;
using FinalChat.DAL.Interfaces;
using FinalChat.DAL.Repositories;
using FinalChat.WEB.Helpers;
using FinalChat.WEB.Hubs;
using FinalChat.WEB.TokenProvider;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;

namespace FinalChat.WEB
{
    public class Startup
    {
        //Секретный ключ 
        private static readonly string secretKey = "mysupersecret_WALKERADMIN!123";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {

            var connection = Configuration["ConnectionStrings:DefaultConnection"];
            services.AddDbContext<ApplicationContext>(
                options => options.UseMySql(connection,
                    mysqlOptions =>
                    {
                        mysqlOptions.ServerVersion(new Version(8, 0, 12), ServerType.MySql);
                    }
            ));

            //string connection = Configuration.GetConnectionString("SQLSERVERCONNECTION");
            //services.AddDbContext<ApplicationContext>(options =>
            //    options.UseSqlServer(connection),ServiceLifetime.Transient);
            services.AddSignalR();

            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IFriendsService, FriendsService>();
            services.AddTransient<IFriendsInvitesService, FriendsInviteService>();
            services.AddTransient<ILocksService, LocksService>();
            services.AddTransient<IMessageService, MessageService>();
            services.AddTransient<IDialogsService, DialogsService>();



            //Шифруем ключ
            var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secretKey));

            //Объект для валидации
            var tokenValidationParameters = new TokenValidationParameters
            {
                // The signing key must match!
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = signingKey,

                // Validate the JWT Issuer (iss) claim
                ValidateIssuer = true,
                ValidIssuer = "ExampleIssuer",

                // Validate the JWT Audience (aud) claim
                ValidateAudience = true,
                ValidAudience = "ExampleAudience",

                // Validate the token expiry
                ValidateLifetime = true,

                // If you want to allow a certain amount of clock drift, set that here:
                ClockSkew = TimeSpan.Zero
            };


            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
            {
                options.ForwardDefaultSelector = ctx =>
                {
                    if (ctx.Request.Path.StartsWithSegments("/api"))
                    {
                        return JwtBearerDefaults.AuthenticationScheme;
                    }
                    else
                    {
                        return CookieAuthenticationDefaults.AuthenticationScheme;
                    }
                };
                options.Cookie.Name = "access_token";
                options.TicketDataFormat = new CustomJwtDataFormat(SecurityAlgorithms.HmacSha256,tokenValidationParameters);
                        })
            .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme,options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.TokenValidationParameters = tokenValidationParameters;
                }); ;

            services.AddMvc();
            Mapper.Initialize(cfg => { });

        }
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            loggerFactory.AddConsole(LogLevel.Debug);
            loggerFactory.AddDebug();

            ConfigureAuth(app);

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Account}/{action=Login}/{id?}");
            });

            app.UseSignalR(routes =>
            {
                routes.MapHub<ChatHub>("/chat");
            });
        }

        private void ConfigureAuth(IApplicationBuilder app)
        {
            app.UseSimpleTokenProvider(new TokenProviderOptions
            {
                Path = "/Account/Authentication",
                Audience = "ExampleAudience",
                Issuer = "ExampleIssuer",               
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secretKey)), SecurityAlgorithms.HmacSha256),
                IdentityResolver = GetIdentity,              
            });

            app.UseAuthentication();
        }

        private async Task<UserDTO> GetIdentity(string username, string password,IUserService userService)
        {
            return await userService.AuthenticateJWT(username, password);
        }
    }
}
