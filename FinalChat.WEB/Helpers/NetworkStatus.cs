﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinalChat.WEB.Helpers
{
    public enum NetworkStatus
    {
        Offline = 0,
        Online = 1
    }
}
