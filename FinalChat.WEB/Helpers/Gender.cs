﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinalChat.WEB.Helpers
{
    public enum Gender
    {
        Female = 0,
        Male = 1,
        Unmentioned = 2
    }
}
