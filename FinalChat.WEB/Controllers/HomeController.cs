﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FinalChat.WEB.Models;
using FinalChat.DAL.EF;
using Microsoft.AspNetCore.Authorization;
using FinalChat.BBL.Interfaces;
using AutoMapper;
using FinalChat.BBL.DTO;
using FinalChat.WEB.Helpers;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.IO;
using Microsoft.AspNetCore.Http;

namespace FinalChat.WEB.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ApplicationContext db;
        private IUserService userService;
        private IFriendsService friendsService;
        private IFriendsInvitesService friendsInvitesService;
        private ILocksService locksService;
        private IDialogsService dialogsService;
        private IMessageService messageService;

        public HomeController(ApplicationContext _db, IUserService _userService, IFriendsService _friendsService, 
            IFriendsInvitesService _friendsInvitesService,ILocksService _locksService,IDialogsService _dialogsService,IMessageService _messageService)
        {
            userService = _userService;
            friendsService = _friendsService;
            friendsInvitesService = _friendsInvitesService;
            locksService = _locksService;
            dialogsService = _dialogsService;
            messageService = _messageService;
            db = _db;
        }

        public async Task<IActionResult> Index()
        {
            var user = await userService.GetUserByEmail(User.Identity.Name);

            UserVM uservm = Mapper.Map<UserDTO, UserVM>(user, opts =>
            {
                opts.ConfigureMap()
                .ForMember(p => p.AvatarFile, opt => opt.Ignore())
                .ForMember(p => p.OldPassword, opt => opt.Ignore());

            });

            return View(uservm);
        }

       [HttpPost]
        public async Task<IActionResult> Edit(UserVM uservm)
        {
            if (uservm.AvatarFile != null)
            {
                byte[] imageData = null;

                using (var binaryReader = new BinaryReader(uservm.AvatarFile.OpenReadStream()))
                {
                    imageData = binaryReader.ReadBytes((int)uservm.AvatarFile.Length);
                }
                uservm.Avatar = imageData;
            }
            UserDTO user = Mapper.Map<UserVM, UserDTO>(uservm,opts=> {
                if (uservm.Password == null)
                {
                 opts.ConfigureMap().ForMember(p => p.Password, opt => opt.Ignore());
                }
            });
            await userService.UpdateUserAsync(user);
            var jsonResult = new
            {
                avatar = uservm.Avatar,
                firstName = uservm.FirstName,
                lastName = uservm.LastName,
                birthDate = uservm.BirthDate.ToString("dd.MM.yyyy"),
                gender = Enum.GetName(typeof(Gender),uservm.Gender),
                martialStatus = Enum.GetName(typeof(MartialStatus), uservm.MartialStatus)
            };
            return Json(jsonResult);
        }

        [HttpGet]
        public async Task<IActionResult> _SearchOfFriends(string phone)
        {
            var user = await userService.GetUserByEmail(User.Identity.Name);
            var result = userService.GetUserByPhone(phone);
            List<UserVM> resultOfsearchVMs = new List<UserVM>();
            foreach (var item in result)
            {
                if (item.ID == user.ID)
                {
                    continue;
                }
                else
                {

                    resultOfsearchVMs.Add(Mapper.Map<UserDTO, UserVM>(item, opts => { opts.ConfigureMap().ForMember(p => p.AvatarFile, opt => opt.Ignore()); }));
                }

            }
            UserRealtionsVM userRelations = new UserRealtionsVM()
            {
                searchResult = resultOfsearchVMs,
                friends = GetAllFriendsVMs(user.ID),
                outcomingInvites = GetOutComingInvitesVMs(user.ID),
                incomingInvites = GetIncomingInvitesVMs(user.ID),
                blackListIDs = GetUsersBlocksList(user.ID),
                blocksofUserIDs = GetBlocksOfUser(user.ID)
            };         
            return PartialView("_SearchOfFriends", userRelations);
        }

        [HttpGet]
        public async Task<IActionResult> GetUserDialogsAsync()
        {
          var user = await userService.GetUserByEmail(User.Identity.Name);
          var dialogs = GetUserDialogsVMs(user.ID);
          return Json(dialogs);
        }

        public async Task<IActionResult> CheckDialogExists(int id)
        {
            var user = await userService.GetUserByEmail(User.Identity.Name);
            var dialog = dialogsService.CheckDialogExists(user.ID, id);
            if (dialog != null)
            {
                return Json(new {dialogExist=true,dialogID=dialog.ID});
            }

                DialogDTO newdialog = new DialogDTO { FirstUserID = user.ID, SecondUserID = id };
                await dialogsService.AddDialogAsync(newdialog);
                var checkNewDialog = dialogsService.CheckDialogExists(user.ID, id);
                await messageService.SendMessageAsync(new MessageDTO {
                    DialogID = checkNewDialog.ID,
                    FromID= user.ID,
                    ToID=id,
                    MessageDate=DateTime.Now,
                    MessageText="Let's talk!",
                    Written=false,
                    FilePath=null
                });
                return Json(new { dialogExist = false, dialogID = checkNewDialog.ID });
        }

        [HttpGet]
        public IActionResult GetAllDialogMessages(int id,int userID)
        {
            
            var dialogMessages = messageService.GetAllDialogMessages(id,userID);
            return Json(dialogMessages);
        }

        [HttpGet]
        public async Task<IActionResult> GetAllFriendsAsync()
        {
           var user = await userService.GetUserByEmail(User.Identity.Name);
           var allFriends = GetAllFriendsVMs(user.ID);
           return Json(allFriends);
        }

        [HttpPost]
        public async Task<IActionResult>RemoveFriendAsync(int id)
        {
            var user = await userService.GetUserByEmail(User.Identity.Name);
            await friendsService.DeleteFriendsAsync(user.ID, id);

            return Ok();
        }


        [HttpGet]
        public async Task<IActionResult> GetIncomingInvitesAsync()
        {
            var user = await userService.GetUserByEmail(User.Identity.Name);
            var incomInvites = GetIncomingInvitesVMs(user.ID);

            return Json(incomInvites);
        }     

        [HttpGet]
        public async Task<IActionResult> GetOutComingInvitesAsync()
        {
            var user = await userService.GetUserByEmail(User.Identity.Name);
            var outInvites = GetOutComingInvitesVMs(user.ID);

            return Json(outInvites);
        }

        [HttpGet]
        public async Task<IActionResult> GetUsersBlackList()
        {
            var user = await userService.GetUserByEmail(User.Identity.Name);
            var blackList = GetUsersBlocksList(user.ID);

            return Json(blackList);
        }

        [HttpPost]
        public async Task<IActionResult> SendInviteAsync(int id)
        {
            var user = await userService.GetUserByEmail(User.Identity.Name);

            FriendInvitesDTO invite = new FriendInvitesDTO()
            {
                FromID = user.ID,
                ToID = id
            };
            await friendsInvitesService.SendInviteAsync(invite);
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> AcceptInviteAjax(int id)
        {
            var user = await userService.GetUserByEmail(User.Identity.Name);

            var inviteExist = friendsInvitesService.CheckInviteExist(id, user.ID);
            if (inviteExist)
            {
                FriendsDTO friends = new FriendsDTO()
                {
                    UserID = user.ID,
                    UserFriendID = id
                };
                FriendsDTO friends2 = new FriendsDTO()
                {
                    UserID = id,
                    UserFriendID = user.ID
                };

                await friendsService.AddFriendsAsync(friends);
                await friendsService.AddFriendsAsync(friends2);
                await friendsInvitesService.DeleteInviteAsync(id, user.ID);
            }

            return Ok();

        }

        [HttpPost]
        public async Task<IActionResult>DeclineInviteAjax(int id)
        {
            var user = await userService.GetUserByEmail(User.Identity.Name);
            await friendsInvitesService.DeleteInviteAsync(id, user.ID);
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> CancelInviteAjax(int id)
        {
            var user = await userService.GetUserByEmail(User.Identity.Name);
            await friendsInvitesService.DeleteInviteAsync(user.ID, id);
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult>BlockUserAsync(int id)
        {
            var user = await userService.GetUserByEmail(User.Identity.Name);
            LockDTO block = new LockDTO()
            {
                LockFromID = user.ID,
                LockToID = id
            };
            await locksService.BlockUserAsync(block);
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult>RemoveBlockOfUserAsync(int id)
        {
            var user = await userService.GetUserByEmail(User.Identity.Name);
            await locksService.RemoveBlockOfUserAsync(user.ID, id);
            return Ok();
        }


        [HttpGet]
        public async Task<IActionResult> _ShowUserProfile(string email)
        {
            var mainUser= await userService.GetUserByEmail(User.Identity.Name);
            var user = await userService.GetUserByEmail(email);
            UserVM uservm = Mapper.Map<UserDTO, UserVM>(user, opts =>
            {
                opts.ConfigureMap()
                .ForMember(p => p.AvatarFile, opt => opt.Ignore());
            });
            UserRealtionsVM userRelations = new UserRealtionsVM()
            {
                User=uservm,
                friends = GetAllFriendsVMs(mainUser.ID),
                outcomingInvites = GetOutComingInvitesVMs(mainUser.ID),
                incomingInvites = GetIncomingInvitesVMs(mainUser.ID),
                blackListIDs = GetUsersBlocksList(mainUser.ID),
                blocksofUserIDs = GetBlocksOfUser(mainUser.ID)
            };

            return PartialView("_UserProfile", userRelations);
        }








        private IEnumerable<UserVM> GetAllFriendsVMs(int id)
        {
            var friends = friendsService.GetAllFriends(id);
            List<UserDTO> userFriendsDTO = new List<UserDTO>();
            List<UserVM> userFriendsVMs = new List<UserVM>();

            foreach (var fr in friends)
            {
                var a = userService.GetUserByID(fr.UserFriendID);
                if (a != null)
                {
                    userFriendsDTO.Add(a);
                }
            }
            foreach (var item in userFriendsDTO)
            {
                userFriendsVMs.Add(Mapper.Map<UserDTO, UserVM>(item, opts => { opts.ConfigureMap().ForMember(p => p.AvatarFile, opt => opt.Ignore()); }));
            }
            return userFriendsVMs;
        }
        
        private IEnumerable<UserVM> GetIncomingInvitesVMs(int id)
        {
            var allUsers = userService.GetAllUsers();
            var incomingInvites = friendsInvitesService.GetInComingInvites(id);

            List<UserVM> userincomingInvitesVMs = new List<UserVM>();
            foreach (var us in allUsers)
            {
                foreach (var inv in incomingInvites)
                {
                    if (us.ID == inv.FromID)
                    {
                        userincomingInvitesVMs.Add(Mapper.Map<UserDTO, UserVM>(us, opts => { opts.ConfigureMap().ForMember(p => p.AvatarFile, opt => opt.Ignore()); }));
                    }
                }
            }

            return userincomingInvitesVMs;
        }

        private IEnumerable<UserVM> GetOutComingInvitesVMs(int id)
        {
            var allUsers = userService.GetAllUsers();
            var outcomingInvites = friendsInvitesService.GetOutComingInvites(id);

            List<UserVM> userOutComingInvitesVMs = new List<UserVM>();
            foreach (var us in allUsers)
            {
                foreach (var inv in outcomingInvites)
                {
                    if (us.ID == inv.ToID)
                    {
                        userOutComingInvitesVMs.Add(Mapper.Map<UserDTO, UserVM>(us, opts => { opts.ConfigureMap().ForMember(p => p.AvatarFile, opt => opt.Ignore()); }));
                    }
                }
            }

            return userOutComingInvitesVMs;
        }

        private IEnumerable<int>GetUsersBlocksList(int id)
        {
            var allUsers = userService.GetAllUsers();
            var usersBlocks = locksService.GetUserBlockList(id);

            List<int> usersBlocksIDs = new List<int>();
            foreach (var us in allUsers)
            {
                foreach (var block in usersBlocks)
                {
                    if (us.ID == block.LockToID)
                    {
                        usersBlocksIDs.Add(us.ID);
                    }
                }
            }

            return usersBlocksIDs;
        }

        private IEnumerable<int>GetBlocksOfUser(int id)
        {
            var allUsers = userService.GetAllUsers();
            var blocksOfUser = locksService.GetBlocksOfUser(id);

            List<int> BlocksOfUsersIDs = new List<int>();
            foreach (var us in allUsers)
            {
                foreach (var inv in blocksOfUser)
                {
                    if (us.ID == inv.LockFromID)
                    {
                        BlocksOfUsersIDs.Add(us.ID);
                    }
                }
            }

            return BlocksOfUsersIDs;
        }

        private IEnumerable<DialogVM>GetUserDialogsVMs(int id)
        {
            var allUsers = userService.GetAllUsers();
            var dialogs = dialogsService.GetAllDialogsOfUser(id);

            List<DialogVM> userDialogsVMs = new List<DialogVM>();
            foreach (var us in allUsers)
            {
                if (us.ID == id)
                {
                    continue;
                }
                foreach (var dialog in dialogs)
                {
                    var lastDialogMessage = messageService.GetLastDialogMessage(dialog.ID);
                    if (us.ID == dialog.FirstUserID || us.ID==dialog.SecondUserID)
                    {
                        userDialogsVMs.Add(new DialogVM { ID = dialog.ID, CompanionAvatar = us.Avatar,
                            LastmessageToID=lastDialogMessage.ToID,
                            CompanionFirstName = us.FirstName,
                            LastMessage = lastDialogMessage.MessageText,
                            LastMessageWritten =lastDialogMessage.Written,
                            LastMessageTime =lastDialogMessage.MessageDate.ToString("dd.MM.yyyy HH:mm"),
                            CompanionID=us.ID
                        });
                    }
                }
            }

            return userDialogsVMs;
        }

        public async Task<IActionResult> CheckOldPassword(string oldPassword)
        {
            var user = await userService.GetUserByEmail(User.Identity.Name);

            var result =userService.CheckOldPassword(user.Email,oldPassword);
            return Json(result);
        }


        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";
            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
