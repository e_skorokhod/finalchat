﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using AutoMapper;
using FinalChat.BBL.DTO;
using FinalChat.BBL.Interfaces;
using FinalChat.WEB.Helpers;
using FinalChat.WEB.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace FinalChat.WEB.Controllers
{
    [AllowAnonymous]
    public class AccountController : Controller
    {
        private IUserService _userService;
        private readonly AppSettings _appSettings;

        public AccountController(IUserService userService, IOptions<AppSettings> appSettings)
        {
            _userService = userService;
            _appSettings = appSettings.Value;
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpGet]
        public async Task<JsonResult> CheckUserPassword(string json)
        {
            JsonLogin jsonData = JsonConvert.DeserializeObject<JsonLogin>(json);

            string email =jsonData.Email;
            string password = jsonData.Password;
            var result = await _userService.Authenticate(email, password);

            return Json(result);
        }

        [HttpGet]
        public async Task<JsonResult> CheckConfirmOfEmial(string json)
        {
            JsonLogin jsonData = JsonConvert.DeserializeObject<JsonLogin>(json);

            string email = jsonData.Email;
            string password = jsonData.Password;
            var result = await _userService.CheckConfirmOfEmail(email, password);

            return Json(result);
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterVM rgvm)
        {
            if (ModelState.IsValid)
            {
                UserDTO user = Mapper.Map<RegisterVM, UserDTO>(rgvm,opts=>
                {
                    opts.ConfigureMap()
                    .ForMember(p => p.ID, opt => opt.Ignore())
                    .ForMember(p => p.ConfirmEmail, opt => opt.Ignore())
                    .ForMember(p => p.PasswordHash, opt => opt.Ignore())
                    .ForMember(p => p.PasswordSalt, opt => opt.Ignore())
                    .ForMember(p => p.BirthDate, opt => opt.Ignore())
                    .ForMember(p => p.Gender, opt => opt.Ignore())
                    .ForMember(p => p.Avatar, opt => opt.Ignore())
                    .ForMember(p => p.NetworkStatus, opt => opt.Ignore())
                    .ForMember(p => p.RegistrationDate, opt => opt.Ignore())
                    .ForMember(p => p.MartialStatus, opt => opt.Ignore());
                });
                await _userService.CreateUserAsync(user);
                return View("DisplayEmail");
            }
            return View(rgvm);

        }



        public async Task<IActionResult> CheckEmailExist(string email)
        {
            var result = await _userService.CheckEmailExists(email);
            return Json(result);
        }
        public async Task<IActionResult>CheckPhoneNumberExists(string phone)
        {
            var result = await _userService.CheckPhoneNumberExists(phone);
            return Json(result);
        }


        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string userEmail,string hash)
        {
            var user = await _userService.GetUserByEmail(userEmail);
            if (user == null && hash==null)
            {
                return View("Error");
            }
            var result = await _userService.ConfirmEmailAsync(user.Email,hash);
            return View(result ? "ConfirmEmail" : "Error");
        }

        public async Task<IActionResult> SignOut()
        {
           await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
           return RedirectToAction("Login");
        }
    }
}