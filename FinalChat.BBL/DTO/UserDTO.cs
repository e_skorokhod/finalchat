﻿using FinalChat.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinalChat.BBL.DTO
{
    public class UserDTO
    {
        public int ID { get; set; }

        public string Login { get; set; }

        public string Email { get; set; }

        public bool ConfirmEmail { get; set; }

        public string Password { get; set; }

        public byte[] PasswordHash { get; set; }

        public byte[] PasswordSalt { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime BirthDate { get; set; }

        public string Phone { get; set; }

        public byte[] Avatar { get; set; }
      
        public DateTime RegistrationDate { get; set; } = DateTime.Now;



        public bool NetworkStatus { get; set; }
        public int Gender { get; set; }  
        public int MartialStatus { get; set; }
    }
}
