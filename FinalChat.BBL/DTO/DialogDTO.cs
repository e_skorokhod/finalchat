﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalChat.BBL.DTO
{
    public class DialogDTO
    {
        public int ID { get; set; }

        public int FirstUserID { get; set; }

        public int SecondUserID { get; set; }
    }
}
