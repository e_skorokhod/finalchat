﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalChat.BBL.DTO
{
   public class FriendInvitesDTO
    {
        public int ID { get; set; }

        public int FromID { get; set; }

        public int ToID { get; set; }

    }
}
