﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalChat.BBL.DTO
{
    public class MessageDTO
    {
        public int ID { get; set; }

        public int FromID { get; set; }

        public int ToID { get; set; }

        public string UserFromName { get; set; }

        public string UserToName { get; set; }

        public string MessageText { get; set; }

        public DateTime MessageDate { get; set; }

        public byte[] UserFromPhoto { get; set; }

        public byte[] UserToPhoto { get; set; }

        public string FilePath { get; set; }

        public bool Written { get; set; }

        public int DialogID { get; set; }
    }
}
