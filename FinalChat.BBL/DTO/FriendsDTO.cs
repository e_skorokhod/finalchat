﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalChat.BBL.DTO
{
    public class FriendsDTO
    {
        public int ID { get; set; }

        public int UserID { get; set; }

        public int UserFriendID { get; set; }
    }
}
