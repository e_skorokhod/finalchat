﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalChat.BBL.DTO
{
    public class LockDTO
    {
        public int ID { get; set; }

        public int LockFromID { get; set; }

        public int LockToID { get; set; }
    }
}
