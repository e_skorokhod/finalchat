﻿using AutoMapper;
using FinalChat.BBL.DTO;
using FinalChat.BBL.Interfaces;
using FinalChat.DAL.Entities;
using FinalChat.DAL.Interfaces;
using FinalChat.DAL.Repositories;
using MimeKit;
using MailKit.Net.Smtp;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace FinalChat.BBL.Services
{
    public class UserService : IUserService
    {
        IUnitOfWork db;
        public UserService(IUnitOfWork _db)
        {
            db = _db;
        }

        public async Task CreateUserAsync(UserDTO userDTO)
        {
            byte[] passwordHash, passwordSalt;
            CreatePasswordHash(userDTO.Password, out passwordHash, out passwordSalt);

            userDTO.PasswordHash = passwordHash;
            userDTO.PasswordSalt = passwordSalt;


            await db.UsersRepository.Create(Mapper.Map<UserDTO, ApplicationUser>(userDTO));

            await SendOnEmailAsync(userDTO.Email, userDTO.PasswordHash, userDTO.PasswordSalt);
        }

        public UserDTO GetUserByID(int id)
        {
            var user = db.UsersRepository.Get(id);
            return Mapper.Map<UserDTO>(user);
        }

        public IEnumerable<UserDTO> GetAllUsers()
        {
            List<UserDTO> result = new List<UserDTO>();

            foreach (var item in db.UsersRepository.GetAll())
                result.Add(Mapper.Map<UserDTO>(item));

            return result;
        }

        public async Task DeleteUserAsync(int id)
        {
            await db.UsersRepository.Delete(id);
        }

        public async Task UpdateUserAsync(UserDTO element)
        {
            ApplicationUser toUpdate = db.UsersRepository.Get(element.ID);

            if (element.Password != null)
            {
                byte[] passwordHash, passwordSalt;
                CreatePasswordHash(element.Password, out passwordHash, out passwordSalt);

                element.PasswordHash = passwordHash;
                element.PasswordSalt = passwordSalt;
            }
            if (toUpdate != null)
            {
                toUpdate = Mapper.Map<UserDTO, ApplicationUser>(element, toUpdate);
                await db.UsersRepository.Update(toUpdate);
            }
        }

        public async Task<UserDTO> GetUserByEmail(string email)
        {
            var user = db.UsersRepository.FindSingle(x => x.Email == email);

            return Mapper.Map<ApplicationUser, UserDTO>(user, opts =>
              {
                  opts.ConfigureMap()
                  .ForMember(p => p.Password, opt => opt.Ignore());
              });
        }

        public IEnumerable<UserDTO> GetUserByPhone(string phone)
        {
            List<UserDTO> users = new List<UserDTO>();
            foreach (var item in db.UsersRepository.Find(x => x.Phone.Contains(phone)||x.Email.Contains(phone)))
            {
                users.Add(Mapper.Map<UserDTO>(item));
            }
            return users;
          
        }

        public async Task<bool>CheckEmailExists(string email)
        {
            var user = db.UsersRepository.FindSingle(x => x.Email == email);
            if (user != null)
            {
                return false;
            }
            return true;

        }
        public async Task<bool> CheckPhoneNumberExists(string phone)
        {
            var user = db.UsersRepository.FindSingle(x => x.Phone == phone);
            if (user != null)
            {
                return false;
            }
            return true;

        }

        private async Task SendOnEmailAsync(string userEmail, byte[] hash, byte[] salt = null, string password = null)
        {
            string messageBody = null;
            string messageSubject = null;
            string hashResult = Encoding.UTF8.GetString(hash);
            if (password != null)
            {
                messageBody = password;
                messageSubject = "Восстановление пароля";
            }
            else
            {

                messageBody = "Подтвердите регистрацию, перейдя по ссылке:" + $"<a href=\"http://localhost:49842/Account/ConfirmEmail?userEmail={userEmail}&hash={hashResult}" + "\">Confirm Link</a> ";
                messageSubject = "Подтверждение адреса почтового ящика";
            }

            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("Administration", "walker090295@gmail.com"));
            emailMessage.To.Add(new MailboxAddress("", userEmail));
            emailMessage.Subject = messageSubject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = messageBody
            };
            using (var client = new MailKit.Net.Smtp.SmtpClient())
            {
                //Refactoring: Extract data to the config file
                //You have an data that must be in the config file.
                await client.ConnectAsync("smtp.gmail.com", 587, false);
                await client.AuthenticateAsync("walker090295@gmail.com", "reddevilegor2261999svetaforever1334863");
                await client.SendAsync(emailMessage);
                await client.DisconnectAsync(true);
            }
        }
        public async Task<bool> ConfirmEmailAsync(string email,string hash)
        {
            var user = db.UsersRepository.FindSingle(x => x.Email == email);
            string userhash = Encoding.UTF8.GetString(user.PasswordHash);
            if (userhash==hash || user.Email==email) //здесь должно быть && но почему то хеши не совпадают.
            {
                user.ConfirmEmail = true;
                await db.SaveAsync();
            }

            return user.ConfirmEmail;
        }


        public async Task<bool> Authenticate(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                return false;

            var user = db.UsersRepository.FindSingle(x => (x.Email==username || x.Phone==username) && VerifyPasswordHash(password, x.PasswordHash, x.PasswordSalt));

            if (user != null)
            {
                return true;
            }
            return false;
        }
        public async Task<UserDTO> AuthenticateJWT(string username, string password)
        {
            var user = db.UsersRepository.FindSingle(x => (x.Email == username || x.Phone == username) && VerifyPasswordHash(password, x.PasswordHash, x.PasswordSalt));

            if (user != null)
            {
                return Mapper.Map<ApplicationUser, UserDTO>(user, opts =>
                {
                    opts.ConfigureMap()
                    .ForMember(p => p.Password, opt => opt.Ignore());
                });
            }
            return null;
        }

        public async Task<bool> CheckConfirmOfEmail(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                return false;

            var user = db.UsersRepository.FindSingle(x => (x.Email == username || x.Phone == username) && VerifyPasswordHash(password, x.PasswordHash, x.PasswordSalt));

            if (user!=null && user.ConfirmEmail != false)
            {
                return true;
            }
            return false;
        }

        public bool CheckOldPassword(string email,string oldPassword)
        {
            var user = db.UsersRepository.FindSingle(x => x.Email==email && VerifyPasswordHash(oldPassword, x.PasswordHash, x.PasswordSalt));
            if (user != null)
            {
                return true;
            }
            return false;
        }

        private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");

            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }
        private static bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            if (storedHash.Length != 64) throw new ArgumentException("Invalid length of password hash (64 bytes expected).", "passwordHash");
            if (storedSalt.Length != 128) throw new ArgumentException("Invalid length of password salt (128 bytes expected).", "passwordHash");

            using (var hmac = new System.Security.Cryptography.HMACSHA512(storedSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != storedHash[i]) return false;
                }
            }

            return true;
        }

       
    }
}
