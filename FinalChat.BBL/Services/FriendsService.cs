﻿using AutoMapper;
using FinalChat.BBL.DTO;
using FinalChat.BBL.Interfaces;
using FinalChat.DAL.Entities;
using FinalChat.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FinalChat.BBL.Services
{
    public class FriendsService : IFriendsService
    {
        IUnitOfWork db;
        public FriendsService(IUnitOfWork _db)
        {
            db = _db;
        }


        public async Task AddFriendsAsync(FriendsDTO friendsDTO)
        {
            await db.FriendsRepository.Create(Mapper.Map<FriendsDTO, Friends>(friendsDTO, opts =>
            {
                opts.ConfigureMap()
                .ForMember(p => p.User, opt => opt.Ignore())
                .ForMember(p => p.UserFriend, opt => opt.Ignore());
            }));
        }


       
        public async Task DeleteFriendsAsync(int userID,int friendID)
        {
            IEnumerable<Friends> friends = db.FriendsRepository.Find(x => (x.UserID == userID && x.UserFriendID == friendID) || (x.UserID == friendID && x.UserFriendID == userID));
            foreach(var friend in friends)
            {
                await db.FriendsRepository.Delete(friend.ID);
            }          
        }


        public IEnumerable<FriendsDTO> GetAllFriends(int id)
        {
            List<FriendsDTO> result = new List<FriendsDTO>();

            foreach (var item in db.FriendsRepository.Find(x=>x.UserID==id))
                result.Add(Mapper.Map<FriendsDTO>(item));

            return result;
        }

    }
}
