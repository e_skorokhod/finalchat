﻿using AutoMapper;
using FinalChat.BBL.DTO;
using FinalChat.BBL.Interfaces;
using FinalChat.DAL.Entities;
using FinalChat.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FinalChat.BBL.Services
{
   public  class DialogsService:IDialogsService
    {
        IUnitOfWork db;
        public DialogsService(IUnitOfWork _db)
        {
            db = _db;
        }

        public async Task AddDialogAsync(DialogDTO dialogDTO)
        {

            await db.DialogsRepository.Create(Mapper.Map<DialogDTO, Dialog>(dialogDTO, opts =>
            {
                opts.ConfigureMap()
                .ForMember(p => p.FirstUser, opt => opt.Ignore())
                .ForMember(p => p.SecondUser, opt => opt.Ignore());
            }));
        }

        public async Task DeleteDialogAsync(int firstUserID, int secondUserID)
        {
            Dialog dialog = db.DialogsRepository.FindSingle(x => (x.FirstUserID == firstUserID && x.SecondUserID== secondUserID) || (x.FirstUserID == secondUserID && x.SecondUserID == firstUserID));
            await db.DialogsRepository.Delete(dialog.ID);

        }

        public IEnumerable<DialogDTO> GetAllDialogsOfUser(int id)
        {
            List<DialogDTO> result = new List<DialogDTO>();

            foreach (var item in db.DialogsRepository.Find(x => x.FirstUserID == id || x.SecondUserID==id))
                result.Add(Mapper.Map<DialogDTO>(item));

            return result;
        }

       public DialogDTO CheckDialogExists(int userID, int secondUserID)
        {
            var dialog =  db.DialogsRepository.FindSingle(x => (x.FirstUserID == userID && x.SecondUserID == secondUserID) || (x.FirstUserID == secondUserID && x.SecondUserID == userID));
            if (dialog == null)
            {
                return null;
            }
            return Mapper.Map<Dialog, DialogDTO>(dialog);
        }
    }
}
