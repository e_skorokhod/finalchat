﻿using AutoMapper;
using FinalChat.BBL.DTO;
using FinalChat.BBL.Interfaces;
using FinalChat.DAL.Entities;
using FinalChat.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FinalChat.BBL.Services
{
    public class LocksService:ILocksService
    {
        IUnitOfWork db;
        public LocksService(IUnitOfWork _db)
        {
            db = _db;
        }

        public async Task BlockUserAsync(LockDTO lockDTO)
        {
            await db.LocksRepository.Create(Mapper.Map<LockDTO, Lock>(lockDTO, opts =>
            {
                opts.ConfigureMap()
                .ForMember(p => p.LockTo, opt => opt.Ignore())
                .ForMember(p => p.LockFrom, opt => opt.Ignore());
            }));
        }


        public IEnumerable<LockDTO> GetUserBlockList(int id)
        {
            List<LockDTO> userBlockList = new List<LockDTO>();

            foreach (var item in db.LocksRepository.Find(x => x.LockFromID == id))
                userBlockList.Add(Mapper.Map<LockDTO>(item));
            return userBlockList;
        }
        public IEnumerable<LockDTO> GetBlocksOfUser(int id)
        {
            List<LockDTO> blocksOfUser = new List<LockDTO>();

            foreach (var item in db.LocksRepository.Find(x => x.LockToID == id))
                blocksOfUser.Add(Mapper.Map<LockDTO>(item));

            return blocksOfUser;
        }      

        public async Task RemoveBlockOfUserAsync(int fromID, int toID)
        {
            var block = db.LocksRepository.FindSingle(x => x.LockFromID == fromID && x.LockToID == toID);
            if (block != null)
            {
                await db.LocksRepository.Delete(block.ID);
            }
        }
    }
}
