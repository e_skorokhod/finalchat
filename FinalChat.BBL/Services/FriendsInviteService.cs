﻿using AutoMapper;
using FinalChat.BBL.DTO;
using FinalChat.BBL.Interfaces;
using FinalChat.DAL.Entities;
using FinalChat.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FinalChat.BBL.Services
{
    public class FriendsInviteService : IFriendsInvitesService
    {
        IUnitOfWork db;
        public FriendsInviteService(IUnitOfWork _db)
        {
            db = _db;
        }

        public async Task SendInviteAsync(FriendInvitesDTO friendInvitesDTO)
        {
            await db.FriendsInvitesRepository.Create(Mapper.Map<FriendInvitesDTO, FriendInvites>(friendInvitesDTO, opts =>
            {
                opts.ConfigureMap()
                .ForMember(p => p.To, opt => opt.Ignore())
                .ForMember(p => p.From, opt => opt.Ignore());
            }));
        }

        

        public bool CheckInviteExist(int fromID,int toID)
        {
           var invite = db.FriendsInvitesRepository.FindSingle(x=>x.FromID==fromID && x.ToID==toID);
            if (invite != null)
            {
                return true;
            }

            return false;
        }

        public IEnumerable<FriendInvitesDTO> GetInComingInvites(int id)
        {
            List<FriendInvitesDTO> invites = new List<FriendInvitesDTO>();

            foreach (var item in db.FriendsInvitesRepository.Find(x => x.ToID == id))
                invites.Add(Mapper.Map<FriendInvitesDTO>(item));

            return invites;
        }

        public IEnumerable<FriendInvitesDTO> GetOutComingInvites(int id)
        {
            List<FriendInvitesDTO> invites = new List<FriendInvitesDTO>();

            foreach (var item in db.FriendsInvitesRepository.Find(x => x.FromID == id))
                invites.Add(Mapper.Map<FriendInvitesDTO>(item));

            return invites;
        }

        public async Task DeleteInviteAsync(int fromID, int toID)
        {
          var invite= db.FriendsInvitesRepository.FindSingle(x => x.FromID == fromID && x.ToID == toID);
            if (invite != null)
            {
               await db.FriendsInvitesRepository.Delete(invite.ID);
            }
        }
    }
}
