﻿using AutoMapper;
using FinalChat.BBL.DTO;
using FinalChat.BBL.Interfaces;
using FinalChat.DAL.Entities;
using FinalChat.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace FinalChat.BBL.Services
{
    public class MessageService:IMessageService
    {
        IUnitOfWork db;

        public MessageService(IUnitOfWork _db)
        {
            db = _db;
        }

        public IEnumerable<MessageDTO> GetAllDialogMessages(int dialogID,int userID)
        {

            List<MessageDTO> dialogMessages = new List<MessageDTO>();
            var unwritten = db.MessagesRepository.GetAll().Where(x => x.DialogID == dialogID && x.ToID==userID && x.Written == false);
            foreach(var mes in unwritten)
            {
                mes.Written = true;
            }
            db.SaveAsync();
            var result = db.MessagesRepository.GetAll().Where(x => x.DialogID == dialogID).Select(m => new MessageDTO
            {
                ID = m.Id,
                FromID = (int)m.FromID,
                ToID = (int)m.ToID,
                UserFromName = m.From.FirstName,
                UserToName = m.From.FirstName,
                MessageText = m.MessageText,
                DialogID = (int)m.DialogID,
                FilePath = m.FilePath,
                Written = m.Written,
                MessageDate = m.MessageDate,
                UserFromPhoto = m.From.Avatar,
                UserToPhoto=m.To.Avatar
            });
            return result;
        }

        public MessageDTO GetLastDialogMessage(int dialogID)
        {        
            var lastMessage = db.MessagesRepository.Find(x => x.DialogID == dialogID).OrderBy(x => x.MessageDate).Last();

            return Mapper.Map<Message,MessageDTO>(lastMessage,opts=> {
                opts.ConfigureMap()
               .ForMember(p => p.UserFromName, opt => opt.Ignore())
               .ForMember(p => p.UserToName, opt => opt.Ignore())
                .ForMember(p => p.UserFromPhoto, opt => opt.Ignore())
                 .ForMember(p => p.UserToPhoto, opt => opt.Ignore());
            });
        }

        public async Task SendMessageAsync(MessageDTO messageDTO)
        {

            await db.MessagesRepository.Create(Mapper.Map<MessageDTO, Message>(messageDTO, opts =>
            {
                opts.ConfigureMap()
                .ForMember(p=>p.Dialog,opt=>opt.Ignore())
                .ForMember(p => p.To, opt => opt.Ignore())
                .ForMember(p => p.From, opt => opt.Ignore());
            }));
        }
    }
}
