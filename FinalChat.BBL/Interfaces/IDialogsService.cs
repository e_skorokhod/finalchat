﻿using FinalChat.BBL.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FinalChat.BBL.Interfaces
{
    public interface IDialogsService
    {
        Task AddDialogAsync(DialogDTO dialogDTO);

        IEnumerable<DialogDTO> GetAllDialogsOfUser(int id);

        Task DeleteDialogAsync(int userID, int friendID);

        DialogDTO CheckDialogExists(int userID, int friendID);
    }
}
