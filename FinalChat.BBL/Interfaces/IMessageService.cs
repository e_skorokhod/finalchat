﻿using FinalChat.BBL.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FinalChat.BBL.Interfaces
{
    public interface IMessageService
    {
        Task SendMessageAsync(MessageDTO messageDTO);
        IEnumerable<MessageDTO> GetAllDialogMessages(int dialogID,int userID);
        MessageDTO GetLastDialogMessage(int dialogID);
    }
}
