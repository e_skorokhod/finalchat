﻿using FinalChat.BBL.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FinalChat.BBL.Interfaces
{
    public interface ILocksService
    {
        Task BlockUserAsync(LockDTO lockDTO);
        Task RemoveBlockOfUserAsync(int fromID, int toID);

        IEnumerable<LockDTO> GetUserBlockList(int id);
        IEnumerable<LockDTO> GetBlocksOfUser(int id);
    }
}
