﻿using FinalChat.BBL.DTO;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace FinalChat.BBL.Interfaces
{
   public interface IUserService
    {
        Task<bool> Authenticate(string username, string password);
        Task<UserDTO>AuthenticateJWT(string username, string password);
        Task<bool> CheckConfirmOfEmail(string username, string password);
        Task<bool> CheckEmailExists(string email);
        Task<bool> CheckPhoneNumberExists(string phone);
        bool CheckOldPassword(string email, string oldPassword);
        UserDTO GetUserByID(int id);
        IEnumerable<UserDTO> GetUserByPhone(string phone);
        Task<UserDTO> GetUserByEmail(string email);
        Task CreateUserAsync(UserDTO userDTO);
        Task DeleteUserAsync(int id);
        Task UpdateUserAsync(UserDTO userDTO);
        IEnumerable<UserDTO> GetAllUsers();



       
        Task<bool> ConfirmEmailAsync(string email,string hash);
    }
}
