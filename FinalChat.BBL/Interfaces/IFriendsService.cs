﻿using FinalChat.BBL.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FinalChat.BBL.Interfaces
{
    public interface IFriendsService
    {
        Task AddFriendsAsync(FriendsDTO friendsDTO);

        IEnumerable<FriendsDTO> GetAllFriends(int id);

        Task DeleteFriendsAsync(int userID,int friendID);
    }
}
