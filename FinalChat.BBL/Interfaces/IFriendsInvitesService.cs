﻿using FinalChat.BBL.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FinalChat.BBL.Interfaces
{
    public interface IFriendsInvitesService
    {
        Task SendInviteAsync(FriendInvitesDTO friendInvitesDTO);
        bool CheckInviteExist(int fromID,int toID);
        Task DeleteInviteAsync(int fromID, int toID);

        IEnumerable<FriendInvitesDTO> GetOutComingInvites(int id);
        IEnumerable<FriendInvitesDTO> GetInComingInvites(int id);

    }
}
