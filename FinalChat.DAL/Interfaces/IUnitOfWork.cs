﻿using FinalChat.DAL.Entities;
using FinalChat.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FinalChat.DAL.Interfaces
{
    public interface IUnitOfWork 
    {
        IRepository<ApplicationUser> UsersRepository { get; }
        IRepository<Friends> FriendsRepository { get; }
        IRepository<FriendInvites> FriendsInvitesRepository { get; }
        IRepository<Message> MessagesRepository{ get; }
        IRepository<Lock> LocksRepository { get; }
        IRepository<Dialog> DialogsRepository { get; }

        Task SaveAsync();
    }
}
