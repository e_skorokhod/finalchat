﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalChat.DAL.Entities
{
    public class Message
    {
        public int Id { get; set; }

        public int? FromID { get; set; }
        public virtual ApplicationUser From { get; set; }


        public int? ToID { get; set; }
        public virtual ApplicationUser To { get; set; }


        public string MessageText { get; set; }

        public DateTime MessageDate { get; set; }

        public string FilePath { get; set; }

        public bool Written { get; set; }

        public int? DialogID { get; set; }
        public virtual Dialog Dialog { get; set; }


    }
}
