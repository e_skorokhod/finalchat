﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FinalChat.DAL.Entities
{
    public class FriendInvites
    {
        public int ID { get; set; }

        
        public int? FromID { get; set; }
        public virtual ApplicationUser From { get; set; }

        
        public int? ToID { get; set; }
        public virtual ApplicationUser To { get; set; }

       
    }
}
