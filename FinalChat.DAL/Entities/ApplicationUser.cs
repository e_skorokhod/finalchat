﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinalChat.DAL.Entities
{
    public class ApplicationUser
    {
        public int ID { get; set; }

        public string Login { get; set; }

        public string Email { get; set; }

        public bool ConfirmEmail { get; set; } = false;

        public byte[] PasswordHash { get; set; }

        public byte[] PasswordSalt { get; set; }

        public string Phone { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime BirthDate { get; set; }
      


        public byte[] Avatar { get; set; }
        
        public DateTime RegistrationDate { get; set; }

        public bool NetworkStatus { get; set; }

        public int Gender { get; set; }

        public int MartialStatus { get; set; }

    }
}
