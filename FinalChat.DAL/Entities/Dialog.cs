﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalChat.DAL.Entities
{
    public class Dialog
    {
        public int ID { get; set; }

        public int? FirstUserID { get; set; }
        public virtual ApplicationUser FirstUser { get; set; }


        public int? SecondUserID { get; set; }
        public virtual ApplicationUser SecondUser { get; set; }


        

    }
}
