﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalChat.DAL.Entities
{
   public  class Lock
    {
        public int ID { get; set; }


        public int? LockFromID { get; set; }
        public virtual ApplicationUser LockFrom { get; set; }


        public int? LockToID { get; set; }
        public virtual ApplicationUser LockTo { get; set; }
    }
}
