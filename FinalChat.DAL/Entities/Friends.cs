﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FinalChat.DAL.Entities
{
    public class Friends
    {
        public int ID { get; set; }

        
        public int? UserID { get; set; }
        public virtual ApplicationUser User { get; set; }


        public int? UserFriendID { get; set; }
        public virtual ApplicationUser UserFriend { get; set; }
    }
}
