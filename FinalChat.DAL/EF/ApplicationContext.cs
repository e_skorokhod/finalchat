﻿using FinalChat.DAL.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinalChat.DAL.EF
{   
    public class ApplicationContext :DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options)
           : base(options)
        {
            Database.EnsureCreated();
        }
        public DbSet<ApplicationUser> Users { get; set; }

        public DbSet<Friends> Friends { get; set; }

        public DbSet<FriendInvites> FriendInvites { get; set; }

        public DbSet<Message> Messages { get; set; }

        public DbSet<Lock> Locks { get; set; }

        public DbSet<Dialog> Dialogs { get; set; }

    }
}
