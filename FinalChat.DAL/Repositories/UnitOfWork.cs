﻿using FinalChat.DAL.EF;
using FinalChat.DAL.Entities;
using FinalChat.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FinalChat.DAL.Repositories
{
    public class UnitOfWork:IUnitOfWork
    {
        private ApplicationContext db;
        private UserRepository usersRepository;
        private FriendsRepository friendsRepository;
        private FriendsInvitesRepository friendsInvitesRepository;
        private MessagesRepository messagesRepository;
        private LocksRepository locksRepository;
        private DialogsRepository dialogsRepository;

        public UnitOfWork(ApplicationContext context)
        {
            db = context;
            usersRepository = new UserRepository(db);
            friendsRepository = new FriendsRepository(db);
            friendsInvitesRepository = new FriendsInvitesRepository(db);
            messagesRepository = new MessagesRepository(db);
            locksRepository = new LocksRepository(db);
            dialogsRepository = new DialogsRepository(db);
        }

        public IRepository<ApplicationUser> UsersRepository
        {
            get { return usersRepository ; }
        }

        public IRepository<Friends> FriendsRepository
        {
            get { return friendsRepository; }
        }

        public IRepository<FriendInvites> FriendsInvitesRepository
        {
            get { return friendsInvitesRepository; }
        }
        public IRepository<Message> MessagesRepository
        {
            get { return messagesRepository; }
        }
        public IRepository<Lock>LocksRepository
        {
            get { return locksRepository; }
        }
        public IRepository<Dialog> DialogsRepository
        {
            get { return dialogsRepository; }
        }

        public async Task SaveAsync()
        {
            await db.SaveChangesAsync();
        }

    }
}
