﻿using FinalChat.DAL.EF;
using FinalChat.DAL.Entities;
using FinalChat.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalChat.DAL.Repositories
{
   public class MessagesRepository : IRepository<Message>
    {
        private ApplicationContext Database;
        public MessagesRepository(ApplicationContext context)
        {
            this.Database = context;
        }

        public async Task Create(Message message)
        {
            await Database.Messages.AddAsync(message);
            await Database.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            Message message = Database.Messages.Find(id);
            if (message != null)
                Database.Messages.Remove(message);
            await Database.SaveChangesAsync();
        }

        public IEnumerable<Message> Find(Func<Message, bool> predicate)
        {
            return Database.Messages.Where(predicate).ToList();
        }

        public Message FindSingle(Func<Message, bool> predicate)
        {
            return Database.Messages.SingleOrDefault(predicate);
        }

        public Message Get(int id)
        {
            return Database.Messages.Find(id);
        }

        public IEnumerable<Message> GetAll()
        {
            return Database.Messages.Include(p => p.From).Include(p => p.To);
        }

        public async Task Update(Message item)
        {
            Database.Entry(item).State = EntityState.Modified;
            await Database.SaveChangesAsync();
        }

    }

}
