﻿using FinalChat.DAL.EF;
using FinalChat.DAL.Entities;
using FinalChat.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalChat.DAL.Repositories
{
    public class UserRepository : IRepository<ApplicationUser>
    {
        private ApplicationContext Database;

        public UserRepository(ApplicationContext context)
        {
            this.Database = context;
        }
       
        public ApplicationUser FindSingle(Func<ApplicationUser, bool> predicate)
        {
            return Database.Users.SingleOrDefault(predicate);
        }

        public IEnumerable<ApplicationUser> GetAll()
        {
            return Database.Users;
        }

        public ApplicationUser Get(int id)
        {
            return Database.Users.Find(id);
        }

        public async Task Create(ApplicationUser user)
        {
            await Database.Users.AddAsync(user);
            await Database.SaveChangesAsync();
        }

        public async Task Update(ApplicationUser user)
        {
            Database.Entry(user).State = EntityState.Modified;
            await Database.SaveChangesAsync();
        }

        public IEnumerable<ApplicationUser> Find(Func<ApplicationUser, Boolean> predicate)
        {
            return Database.Users.Where(predicate).ToList();
        }

        public async Task Delete(int id)
        {
            ApplicationUser user = Database.Users.Find(id);
            if (user != null)
                Database.Users.Remove(user);
            await Database.SaveChangesAsync();
        }


    }
}