﻿using FinalChat.DAL.EF;
using FinalChat.DAL.Entities;
using FinalChat.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalChat.DAL.Repositories
{
 
        public class LocksRepository : IRepository<Lock>
        {
            private ApplicationContext Database;
            public LocksRepository(ApplicationContext context)
            {
                this.Database = context;
            }

            public async Task Create(Lock _lock)
            {
                await Database.Locks.AddAsync(_lock);
                await Database.SaveChangesAsync();
            }

            public async Task Delete(int id)
            {
                Lock _lock = Database.Locks.Find(id);
                if (_lock != null)
                    Database.Locks.Remove(_lock);
                await Database.SaveChangesAsync();
            }

            public IEnumerable<Lock> Find(Func<Lock, bool> predicate)
            {
                return Database.Locks.Where(predicate).ToList();
            }

            public Lock FindSingle(Func<Lock, bool> predicate)
            {
                return Database.Locks.SingleOrDefault(predicate);
            }

            public Lock Get(int id)
            {
                return Database.Locks.Find(id);
            }

            public IEnumerable<Lock> GetAll()
            {
                return Database.Locks;
            }

            public async Task Update(Lock item)
            {
                Database.Entry(item).State = EntityState.Modified;
                await Database.SaveChangesAsync();
            }
        }
    }
