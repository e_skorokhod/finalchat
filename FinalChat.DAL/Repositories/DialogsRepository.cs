﻿using FinalChat.DAL.EF;
using FinalChat.DAL.Entities;
using FinalChat.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalChat.DAL.Repositories
{
    public class DialogsRepository:IRepository<Dialog>
    {
        private ApplicationContext Database;
        public DialogsRepository(ApplicationContext context)
        {
            this.Database = context;
        }

        public async Task Create(Dialog dialog)
        {
            await Database.Dialogs.AddAsync(dialog);
            await Database.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            Dialog dialog = Database.Dialogs.Find(id);
            if (dialog != null)
                Database.Dialogs.Remove(dialog);
            await Database.SaveChangesAsync();
        }

        public IEnumerable<Dialog> Find(Func<Dialog, bool> predicate)
        {
            return Database.Dialogs.Where(predicate).ToList();
        }

        public Dialog FindSingle(Func<Dialog, bool> predicate)
        {
            return Database.Dialogs.SingleOrDefault(predicate);
        }

        public Dialog Get(int id)
        {
            return Database.Dialogs.Find(id);
        }

        public IEnumerable<Dialog> GetAll()
        {
            return Database.Dialogs;
        }

        public async Task Update(Dialog item)
        {
            Database.Entry(item).State = EntityState.Modified;
            await Database.SaveChangesAsync();
        }

    }
}
