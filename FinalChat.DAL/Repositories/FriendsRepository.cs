﻿using FinalChat.DAL.EF;
using FinalChat.DAL.Entities;
using FinalChat.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalChat.DAL.Repositories
{
    public class FriendsRepository : IRepository<Friends>
    {
        private ApplicationContext Database;
        public FriendsRepository(ApplicationContext context)
        {
            this.Database = context;
        }

        public async Task Create(Friends item)
        {
            await Database.Friends.AddAsync(item);
            await Database.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {          
           Friends friends = Database.Friends.Find(id);
            if (friends != null)
                Database.Friends.Remove(friends);
            await Database.SaveChangesAsync();
        }

        public IEnumerable<Friends> Find(Func<Friends, bool> predicate)
        {
            return Database.Friends.Where(predicate).ToList();
        }

        public Friends FindSingle(Func<Friends, bool> predicate)
        {
            return Database.Friends.SingleOrDefault(predicate);
        }

        public Friends Get(int id)
        {
            return Database.Friends.Find(id);
        }

        public IEnumerable<Friends> GetAll()
        {
            return Database.Friends;
        }

        public async Task Update(Friends item)
        {
            Database.Entry(item).State = EntityState.Modified;
            await Database.SaveChangesAsync();
        }
       
    }
}
