﻿using FinalChat.DAL.EF;
using FinalChat.DAL.Entities;
using FinalChat.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalChat.DAL.Repositories
{
    public class FriendsInvitesRepository : IRepository<FriendInvites>
    {
        private ApplicationContext Database;
        public FriendsInvitesRepository(ApplicationContext context)
        {
            this.Database = context;
        }

        public async Task Create(FriendInvites item)
        {
            await Database.FriendInvites.AddAsync(item);
            await Database.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
           FriendInvites friendsInvite = Database.FriendInvites.Find(id);
            if (friendsInvite != null)
                Database.FriendInvites.Remove(friendsInvite);
            await Database.SaveChangesAsync();
        }

        public IEnumerable<FriendInvites> Find(Func<FriendInvites, bool> predicate)
        {
            return Database.FriendInvites.Where(predicate).ToList();
        }

        public FriendInvites FindSingle(Func<FriendInvites, bool> predicate)
        {
            return Database.FriendInvites.SingleOrDefault(predicate);
        }

        public FriendInvites Get(int id)
        {
            return Database.FriendInvites.Find(id);
        }

        public IEnumerable<FriendInvites> GetAll()
        {
            return Database.FriendInvites;
        }

        public async Task Update(FriendInvites item)
        {
            Database.Entry(item).State = EntityState.Modified;
            await Database.SaveChangesAsync();
        }
    }
}
